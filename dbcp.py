#!/bin/env python
# dbcp.py
# Copyright (C) 2020  michael mccune
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''dbcp.py a script to copy files to a dropbox account'''
import argparse
from datetime import datetime
import os
import os.path as path
import sys

import dropbox
from dropbox.files import CommitInfo
from dropbox.files import WriteMode
from dropbox.files import UploadSessionCursor


ACCESS_TOKEN_ENV_VAR = 'DROPBOX_ACCESS_TOKEN'


def main(args):
    conn = dropbox.Dropbox(oauth2_access_token=args.token)
    user = conn.users_get_current_account()
    print(f'Connected to account for {user.email}')

    if not path.exists(args.source):
        raise Exception(f'Error: file {args.source} does not exist')

    if path.isdir(args.source):
        raise Exception(f'Error: file {args.source} is a directory')

    one_mb = 1024 * 1024
    sourcesize = path.getsize(args.source)
    cursor = None
    metadata = None
    destination = args.destination
    if destination.endswith('/'):
        destination += path.basename(args.source)
    with open(args.source, 'rb') as sourcefile:
        if args.offset > 0:
            newoffset = sourcefile.seek(args.offset)
            if newoffset != args.offset:
                raise Exception('Error seeking in source file')
        eof = False
        print(f'Copying {args.source} to {destination}')
        now = datetime.now()
        if args.sessionid:
            cursor = UploadSessionCursor(args.sessionid, args.offset)
        while not eof:
            buf = bytearray(one_mb)
            bytes_read = sourcefile.readinto(buf)
            if bytes_read < one_mb:
                buf = buf[:bytes_read]
                eof = True
            if cursor is None:
                result = conn.files_upload_session_start(bytes(buf))
                cursor = UploadSessionCursor(result.session_id, len(buf))
                print(f'Session ID: {result.session_id}')
                # reset the buf in case this is the end of file
                buf = bytearray()
            if eof:
                commit = CommitInfo(path=destination, mode=WriteMode('add', None), autorename=False)
                metadata = conn.files_upload_session_finish(bytes(buf), cursor, commit)
            elif len(buf) > 0:
                conn.files_upload_session_append_v2(bytes(buf), cursor)
                cursor.offset += len(buf)
            then = now
            now = datetime.now()
            diff = now - then
            bytes_per_second = (1 / diff.total_seconds()) * one_mb
            sizetext = f'{cursor.offset} / {sourcesize} -- bps {bytes_per_second}'
            sys.stdout.write(f'\r {" " * len(sizetext)}\r{sizetext}')
    if metadata:
        print(f'\nCopied source to destination ({metadata.id})')
    else:
        print('\nWarning: no metadata, unknown send result')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='copy a file to dropbox')
    parser.add_argument('-t', '--token', help='oauth2 access token')
    parser.add_argument('-s', '--sessionid', help='the session id')
    parser.add_argument('-o', '--offset', help='byte offset in source file', default=0, type=int)
    parser.add_argument('source', help='source file to copy')
    parser.add_argument('destination', help='destination path in dropbox')

    args = parser.parse_args()

    if args.token is None and os.environ.get(ACCESS_TOKEN_ENV_VAR) is None:
        print('Error: no access token specified')
        sys.exit(1)
    elif args.token is None:
        args.token = os.environ[ACCESS_TOKEN_ENV_VAR]

    try:
        main(args)
    except Exception as ex:
        print(f'\nError: {str(ex)}')
        sys.exit(1)
